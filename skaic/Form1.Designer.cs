﻿namespace skaic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btndot = new System.Windows.Forms.Button();
            this.btneq = new System.Windows.Forms.Button();
            this.textbox = new System.Windows.Forms.TextBox();
            this.btnclear = new System.Windows.Forms.Button();
            this.btndal = new System.Windows.Forms.Button();
            this.btndaug = new System.Windows.Forms.Button();
            this.btnmin = new System.Windows.Forms.Button();
            this.btnplus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(11, 90);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(152, 75);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(179, 90);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(152, 75);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(347, 90);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(152, 75);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(347, 171);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(152, 75);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(179, 171);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(152, 75);
            this.btn5.TabIndex = 4;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(11, 171);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(152, 75);
            this.btn4.TabIndex = 3;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(347, 252);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(152, 75);
            this.btn9.TabIndex = 8;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(179, 252);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(152, 75);
            this.btn8.TabIndex = 7;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.button5_Click);
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(11, 252);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(152, 75);
            this.btn7.TabIndex = 6;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.button6_Click);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(11, 333);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(320, 75);
            this.btn0.TabIndex = 9;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btndot
            // 
            this.btndot.Location = new System.Drawing.Point(347, 333);
            this.btndot.Name = "btndot";
            this.btndot.Size = new System.Drawing.Size(69, 75);
            this.btndot.TabIndex = 10;
            this.btndot.Text = ",";
            this.btndot.UseVisualStyleBackColor = true;
            // 
            // btneq
            // 
            this.btneq.Location = new System.Drawing.Point(430, 333);
            this.btneq.Name = "btneq";
            this.btneq.Size = new System.Drawing.Size(69, 75);
            this.btneq.TabIndex = 11;
            this.btneq.Text = "=";
            this.btneq.UseVisualStyleBackColor = true;
            this.btneq.Click += new System.EventHandler(this.btneq_Click);
            // 
            // textbox
            // 
            this.textbox.Location = new System.Drawing.Point(11, 12);
            this.textbox.Name = "textbox";
            this.textbox.Size = new System.Drawing.Size(630, 20);
            this.textbox.TabIndex = 12;
            this.textbox.Text = "0";
            this.textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(11, 38);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(630, 46);
            this.btnclear.TabIndex = 13;
            this.btnclear.Text = "C";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btndal
            // 
            this.btndal.Location = new System.Drawing.Point(529, 90);
            this.btndal.Name = "btndal";
            this.btndal.Size = new System.Drawing.Size(112, 75);
            this.btndal.TabIndex = 14;
            this.btndal.Text = "/";
            this.btndal.UseVisualStyleBackColor = true;
            this.btndal.Click += new System.EventHandler(this.btndal_Click);
            // 
            // btndaug
            // 
            this.btndaug.Location = new System.Drawing.Point(529, 171);
            this.btndaug.Name = "btndaug";
            this.btndaug.Size = new System.Drawing.Size(112, 75);
            this.btndaug.TabIndex = 15;
            this.btndaug.Text = "X";
            this.btndaug.UseVisualStyleBackColor = true;
            this.btndaug.Click += new System.EventHandler(this.btndaug_Click);
            // 
            // btnmin
            // 
            this.btnmin.Location = new System.Drawing.Point(529, 252);
            this.btnmin.Name = "btnmin";
            this.btnmin.Size = new System.Drawing.Size(112, 75);
            this.btnmin.TabIndex = 16;
            this.btnmin.Text = "-";
            this.btnmin.UseVisualStyleBackColor = true;
            this.btnmin.Click += new System.EventHandler(this.btnmin_Click);
            // 
            // btnplus
            // 
            this.btnplus.Location = new System.Drawing.Point(529, 331);
            this.btnplus.Name = "btnplus";
            this.btnplus.Size = new System.Drawing.Size(112, 75);
            this.btnplus.TabIndex = 17;
            this.btnplus.Text = "+";
            this.btnplus.UseVisualStyleBackColor = true;
            this.btnplus.Click += new System.EventHandler(this.btnplus_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 418);
            this.Controls.Add(this.btnplus);
            this.Controls.Add(this.btnmin);
            this.Controls.Add(this.btndaug);
            this.Controls.Add(this.btndal);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.textbox);
            this.Controls.Add(this.btneq);
            this.Controls.Add(this.btndot);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btndot;
        private System.Windows.Forms.Button btneq;
        private System.Windows.Forms.TextBox textbox;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btndal;
        private System.Windows.Forms.Button btndaug;
        private System.Windows.Forms.Button btnmin;
        private System.Windows.Forms.Button btnplus;
    }
}


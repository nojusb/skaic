﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace skaic
{
    public partial class Form1 : Form
    {
        double funkcija1;
        double funkcija2;
        int skaiciavimas;

        public Form1()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "8";
            }
            else
            {
                textbox.Text += 8;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "9";
            }
            else
            {
                textbox.Text += 9;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "7";
            }
            else
            {
                textbox.Text += 7;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "6";
            }
            else
            {
                textbox.Text += 6;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "5";
            }
            else
            {
                textbox.Text += 5;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "4";
            }
            else
            {
                textbox.Text += 4;
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "1";
            }
            else
            {
                textbox.Text += 1;
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "2";
            }
            else
            {
                textbox.Text += 2;
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "3";
            }
            else
            {
                textbox.Text += 3;
            }
        }

        private void btnplus_Click(object sender, EventArgs e)
        {
            funkcija1 = Convert.ToInt64(textbox.Text);
            textbox.Text = "0";
            skaiciavimas = 1;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(textbox.Text) == 0)
            {
                textbox.Text = "0";
            }
            else
            {
                textbox.Text += 0;
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            textbox.Text = "0";
            
        }
        private void btnmin_Click(object sender, EventArgs e)
        {
            funkcija1 = Convert.ToInt64(textbox.Text);
            textbox.Text = "0";
            skaiciavimas = 2;
        }

        private void btndal_Click(object sender, EventArgs e)
        {
            funkcija1 = Convert.ToInt64(textbox.Text);
            textbox.Text = "0";
            skaiciavimas = 4;
        }

        private void btneq_Click(object sender, EventArgs e)
        {
            funkcija2 = Convert.ToInt64(textbox.Text);
            if (skaiciavimas == 4)
            {
                textbox.Text = Convert.ToString(funkcija1 / funkcija2);
            }
            else if (skaiciavimas == 3)
            {
                textbox.Text = Convert.ToString(funkcija1 * funkcija2);
            }
        }

        private void btndaug_Click(object sender, EventArgs e)
        {
            funkcija1 = Convert.ToInt64(textbox.Text);
            textbox.Text = "0";
            skaiciavimas = 3;
        }
    }
}